import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from './_services/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule } from 'ngx-toastr';
import { NetworkInterceptor } from './_services/network.interceptor';

import { GenerateCodeComponent } from './app-shell/generate-code/generate-code.component';
import { LoginWebComponent } from './app-shell/login-web/login-web.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { AvatarModule } from 'primeng/avatar';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { NavbarComponent } from './app-shell/navbar/navbar.component';
import {CaptchaModule} from 'primeng/captcha';
import {BlockUIModule} from 'primeng/blockui';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    LoginWebComponent,
    GenerateCodeComponent,
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-bottom-right',
      tapToDismiss: true,
      maxOpened: 1,
      preventDuplicates: true,
    }),
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    PasswordModule,
    CardModule,
    ButtonModule,
    AvatarModule,
    OverlayPanelModule,
    CaptchaModule,
    BlockUIModule,    
    DropdownModule
  ],
  providers: [
    UserService,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NetworkInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
