import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { CodeAppResponse, Link, User } from '../_models/auth';

@Injectable({
  providedIn: 'root',
})
export class UserService{
  private baseURL: string = environment.baseURL;

  constructor(private http: HttpClient, private cookieService: CookieService) {}

  login(emailOrIdentity: string, password: string): Observable<User> {
    return this.http.post<User>(`${this.baseURL}/api/auth/loginWeb`, {
      emailOrIdentity,
      password,
    });
  }
  generateCode(id:number): Observable<Link> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.cookieService.get('token')}`,
    });

    return this.http.get<Link>(`${this.baseURL}/api/IMEI/generateCode?appId=${id}`, {headers: headers});
  }

  getAppList(): Observable<CodeAppResponse>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.cookieService.get('token')}`,
    });
    return this.http.get<any>(`${this.baseURL}/api/IMEI/getAppList`, {headers: headers});
  }
}
