import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private cookieService: CookieService) {}

  isAuthenticated(): boolean {
    return !!this.cookieService.get('token');
  }
}
