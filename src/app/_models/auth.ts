
export interface User {
  token: string;
  employeeId: string;
  role: string;
  mail: string;
  name: string;
  identity1: string;
  exitDate: string;
  managementPosition: string;
  employeeGroup: string;
  plant: number;
  jobTitle: string;
}
export interface Link {
  iosLink: string;
}

export interface CodeApp {
  id: number,
  appName: string
}

export interface CodeAppResponse {
  appList: CodeApp[];
}