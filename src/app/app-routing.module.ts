import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenerateCodeComponent } from './app-shell/generate-code/generate-code.component';
import { LoginWebComponent } from './app-shell/login-web/login-web.component';
import { AuthGuard } from './_services/auth.guard';

const routes: Routes = [
  { 
    path: '', redirectTo: 'loginWeb', pathMatch: 'full' 
  },
  {
    path: 'loginWeb',
    component: LoginWebComponent,
  },
  {
    path: 'generateCode',
    component: GenerateCodeComponent,
    //canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: 'loginWeb',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash : true})],
  exports: [RouterModule],
})
export class AppRoutingModule {}
