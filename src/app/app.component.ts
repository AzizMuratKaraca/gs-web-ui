import { Component, OnInit } from '@angular/core';
import { LoadingService } from './_services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent{
  loading$= this.loader.laoding$;
  blockedValue = this.loader.laoding$ ? false : true;
  constructor(public loader: LoadingService){
  }
}
