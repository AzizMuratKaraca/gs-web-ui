import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-login-web',
  templateUrl: './login-web.component.html',
  styleUrls: ['./login-web.component.scss'],
})
export class LoginWebComponent implements OnInit {
  siteKey: string = '6Le7PMAaAAAAAPRiEX_GyzGhNrGxZz9uFhePVj7F'
  emailValue: any;
  passwordValue: any;
  passwordCorrect: boolean = true;
  captchaVisibled:boolean=false;
  loginButtonDisabled:boolean=false;
  unsuccesfulLoginCount:number=0;

  constructor(
    private userService: UserService,
    private router: Router,
    private cookieService: CookieService,
    private toastr: ToastrService, 
  ) {}
  
  ngOnInit() {
    this.cookieService.deleteAll();    
  }

  login() {
    this.userService.login(this.emailValue, this.passwordValue).subscribe(
      (res) => {
        this.cookieService.set('token', res.token);
        this.cookieService.set('mail', res.mail);
        this.cookieService.set('name', res.name);
        this.cookieService.set('managementPosition', res.managementPosition);
        this.router.navigateByUrl('/generateCode');
      },
      (err) => {
        this.unsuccesfulLoginCount++;   
        this.captchaVisibled = this.unsuccesfulLoginCount>=3;
        this.loginButtonDisabled = this.unsuccesfulLoginCount===3;
            
        this.toastr.error(
          'Eposta veya şifreyi hatalı girdiniz',
          'Hatalı İşlem!'
        );
      }
    );
  }

  showResponse(event:any){    
    this.loginButtonDisabled = !event?.response;
  }
}
