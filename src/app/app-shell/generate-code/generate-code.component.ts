import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { CodeApp } from 'src/app/_models/auth';

@Component({
  selector: 'app-generate-code',
  templateUrl: './generate-code.component.html',
  styleUrls: ['./generate-code.component.scss'],
})
export class GenerateCodeComponent implements OnInit {
  isButtonDisabled: boolean = false;
  generateCodeInput: string = '';
  click: string = 'Click';
  mailValue!: string;
  codeApp!: CodeApp;

  constructor(
    private userService: UserService,
    private cookieService: CookieService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {

    this.mailValue = this.cookieService.get('mail');
    if (this.cookieService.check('expireNewCode')) {
      const expireTime = parseInt(this.cookieService.get('expireNewCode'));
      this.isButtonDisabled = true;
      this.toastr.info(
        'Yeni kod için ' + Math.round((expireTime - (new Date()).getTime()) / 1000) + ' saniye bekleyiniz.',
        'Bilgilendirme!'
      );
      this.disableButton();
      return;
    }
  }

  get3MinutesLater(): Date {
    const now = new Date();
    const min3Later = new Date();
    min3Later.setTime(now.getTime() + (180 * 1000));
    return min3Later;
  }

  disableButton(): void {
    this.isButtonDisabled = true;
    const expireTime = parseInt(this.cookieService.get('expireNewCode'));

    setTimeout(() => {
      this.isButtonDisabled = false;
    }, expireTime - (new Date()).getTime());
  }

  generateCode() {
    this.userService.generateCode(this.codeApp?.id).subscribe(
      (res) => {
        this.generateCodeInput = res.iosLink;
        this.cookieService.set('expireNewCode', this.get3MinutesLater().getTime().toString(), this.get3MinutesLater());
        this.disableButton();
      }
    );
  }

  onSelectedProject(codeApp: CodeApp) {
    this.codeApp = codeApp;
  }
}
