import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { CodeApp } from 'src/app/_models/auth';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {

  @Output() selectedProject = new EventEmitter<any>();

  loggedInUser: string = '';
  managementPositionInUser: string = '';

  appList: CodeApp[] = [];

  constructor(
    private cookieService: CookieService,
    private userService: UserService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.loggedInUser = this.cookieService.get('name');
    this.managementPositionInUser = this.cookieService.get('managementPosition');

    this.userService.getAppList().subscribe((res) => {
      if (!res || !res.appList || res.appList.length == 0) {
        return;
      }
      this.selectedProject.emit(res.appList[0]);
      this.appList = res.appList;
    });
  }

  signOut() {
    this.router.navigateByUrl('/loginWeb');
    this.cookieService.deleteAll();
  }

  changeSelected(event: any) {
    this.selectedProject.emit(event.value);
  }
}
